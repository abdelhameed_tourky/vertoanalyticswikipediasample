//
//  WikipediaAPIAction.swift
//  WikiPicia
//
//  Created by AMTourky on 2/4/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import Cocoa
import CoreLocation
import Alamofire

class WikipediaAPIAction: NSObject
{
    var baseURL: String = "https://en.wikipedia.org/w/api.php"
    var format: String = "json"
    var action: String
    
    init(withAction action: String)
    {
        self.action = action
    }
    
    func getArticlesAroundCoordinate(coordinate: CLLocationCoordinate2D, inRadius radius: Int = 10000,inList list: String = "geosearch", withLimit limit: Int = 50, andCallback callback: (error: NSError?, articles: [Article]?) -> Void)
    {
        var parameters = [String: String]()
        parameters["action"] = self.action
        parameters["list"] = list
        parameters["gslimit"] = String(limit)
        parameters["format"] = self.format
        parameters["gscoord"] = String(coordinate.latitude)+"|"+String(coordinate.longitude)
        parameters["gsradius"] = String(radius)
        
        
        Alamofire.request(.GET, self.baseURL, parameters: parameters)
            .responseJSON() { response in
                if let anError = response.result.error
                {
                    callback(error: anError, articles: nil)
                }
                else
                {
                    if let JSONResponse = response.result.value as? [String: AnyObject], articlesArr = self.articlesFromResult(JSONResponse)
                    {
                        callback(error: nil, articles: articlesArr)
                    }
                    else
                    {
                        let error = NSError(domain: "Wikipedia API Service", code: 1, userInfo: [NSLocalizedDescriptionKey: "Weird, no articles found! are you in a galaxy far far away from The Milky Way?! \nyaaaaaaa, I knew aliens exist, I knew it!\nI came in peace http://amtourky.me"])
                        callback(error: error, articles: nil)
                    }
                }
        }
    }
    
    func articlesFromResult(result: [String: AnyObject]) -> [Article]?
    {
        if let queryResult = result["query"] as? [String: AnyObject], articlesResults = queryResult["geosearch"] as? [[String: AnyObject]] where articlesResults.count > 0
        {
            var articlesArr = [Article]()
            for articleResult in articlesResults
            {
                if let theArticle = self.articleFromArticleResult(articleResult)
                {
                    articlesArr.append(theArticle)
                }
            }
            return articlesArr
        }
        return nil
    }
    
    func articleFromArticleResult(articleResult: [String: AnyObject]) -> Article?
    {
        if let theTitle = articleResult["title"] as? String , thePageID = articleResult["pageid"] as? Int
        {
            return Article(withPageID: String(thePageID), andTitle: theTitle)
        }
        return nil
    }
    
    
    func getImagesOfArticles(articles: [Article], andCallback callback: (error: NSError?, images: [Image]?) -> Void)
    {
        var allImagesArr = [Image]()
        
        var parameters = [String: String]()
        parameters["action"] = self.action
        parameters["format"] = self.format
        parameters["prop"] = "images"
        parameters["imlimit"] = "max"
        var pageIDs = ""
        for article in articles
        {
            pageIDs += article.pageID+"|"
        }
        parameters["pageids"] = pageIDs
        
        
        var theCompletionHandler: ( (Response<AnyObject, NSError>) -> Void )? = nil
        
        let completionHandler: (Response<AnyObject, NSError>) -> Void = { response in
            if let anError = response.result.error
            {
                callback(error: anError, images: nil)
            }
            else
            {
                if let JSONResponse = response.result.value as? [String: AnyObject], imagesArr = self.imagesFromResult(JSONResponse)
                {
                    allImagesArr.appendContentsOf(imagesArr)
                    print("got images: ", imagesArr.count)
                    if let batchComplete = JSONResponse["batchcomplete"] as? String where batchComplete == ""
                    {
                        callback(error: nil, images: allImagesArr)
                    }
                    else if let imageContinue = JSONResponse["continue"] as? [String: String], imContinueValue = imageContinue["imcontinue"] where imContinueValue != ""
                    {
                        var innerParameters = [String: String]()
                        for (key, value) in parameters
                        {
                            innerParameters[key] = value
                        }
                        innerParameters["imcontinue"] = imContinueValue
                        Alamofire.request(.GET, self.baseURL, parameters: innerParameters)
                            .responseJSON(completionHandler: theCompletionHandler!)
                    }
                    
                    
                }
                else
                {
                    let error = NSError(domain: "Wikipedia API Service", code: 2, userInfo: [NSLocalizedDescriptionKey: "Weird, no Images found, looks like article writers around you don't believe in 'A picture is worth a thousand words'!"])
                    callback(error: error, images: nil)
                }
            }
        }
        theCompletionHandler = completionHandler
        
        Alamofire.request(.GET, self.baseURL, parameters: parameters)
            .responseJSON(completionHandler: completionHandler)
    }

    
    func imagesFromResult(result: [String: AnyObject]) -> [Image]?
    {
        if let queryResult = result["query"] as? [String: AnyObject], imagesResults = queryResult["pages"] as? [String: AnyObject]
        {
            var imagesArr = [Image]()
            for (_, page) in imagesResults
            {
                if let theImagesResults = page["images"] as? [[String: AnyObject]]
                {
                    for imageResult in theImagesResults
                    {
                        if let theImage = self.imageFromImageResult(imageResult)
                        {
                            imagesArr.append(theImage)
                        }
                    }
                }
            }
            return imagesArr
        }
        return nil
    }
    
    func imageFromImageResult(imageResult: [String: AnyObject]) -> Image?
    {
        if let theTitle = imageResult["title"] as? String
        {
            return Image(withTitle: theTitle)
        }
        return nil
    }
}
