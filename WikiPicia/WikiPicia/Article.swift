//
//  Article.swift
//  WikiPicia
//
//  Created by AMTourky on 2/4/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import Cocoa

class Article: NSObject
{
    var pageID: String
    var title: String
    
    init(withPageID pageID: String, andTitle title: String)
    {
        self.pageID = pageID
        self.title = title
    }
}
