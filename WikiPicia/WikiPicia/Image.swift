//
//  Image.swift
//  WikiPicia
//
//  Created by AMTourky on 2/4/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import Cocoa

class Image: NSObject
{
    var title: String
    
    init(withTitle title: String)
    {
        self.title = title
    }
    
    var tearedTitle: String
    {
        var tempTitle = String(self.title)
        tempTitle = self.removePrefixesFrom(tempTitle)
        tempTitle = self.removeSuffixesFrom(tempTitle)
        tempTitle = self.removeSpecialCharactersFrom(tempTitle)
        
        return tempTitle
    }
    
    func removeSuffixesFrom(s: String) -> String
    {
        var tempTitle = String(s)
        tempTitle = self.removeExtensionFrom(tempTitle)
        let suffixesList = ["ing", "ed", "ism", "er", "uan", "less", "tion"]
        for suffix in suffixesList
        {
            if let range = tempTitle.rangeOfString(suffix, options: NSStringCompareOptions.BackwardsSearch) where range.endIndex == tempTitle.characters.endIndex
            {
                return tempTitle.stringByReplacingCharactersInRange(range, withString: "")
            }
        }
        return tempTitle
    }
    
    func removeExtensionFrom(s: String) -> String
    {
        return (s as NSString).stringByDeletingPathExtension
    }
    
    func removePrefixesFrom(s: String) -> String
    {
        return s.stringByReplacingOccurrencesOfString("File:", withString: "")
    }
    
    func removeSpecialCharactersFrom(s: String) -> String
    {
        var tempTitle = s
        let specialChars = ["-", "_", ".", "(", ")", "[", "]", "'", "\"", "@", "#", "$", "%", "&", "*", ":", ";", ",", "|"]
        for char in specialChars
        {
            tempTitle = tempTitle.stringByReplacingOccurrencesOfString(char, withString: " ")
        }
        return tempTitle
        
    }
}
