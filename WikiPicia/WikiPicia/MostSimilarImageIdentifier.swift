//
//  MostSimilarImageIdentifier.swift
//  WikiPicia
//
//  Created by AMTourky on 2/5/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import Cocoa

class MostSimilarImageIdentifier: NSObject
{
    var images: [Image]
    var titlesField: [[String]]
    
    init(withImages images: [Image])
    {
        self.images = images
        self.titlesField = [[String]]()
    }
    
    func theMostSimilarImage() -> Image
    {
        self.buildTitlesField()
        let index = self.mostSimilarIndex()
        print(self.images[index].title)
        return self.images[index]
    }
    
    func buildTitlesField()
    {
        for image in self.images
        {
            let title = image.tearedTitle.lowercaseString
            let splittedWords = title.characters.split{$0 == " "}.map(String.init)
            self.titlesField.append(splittedWords)
        }
    }
    
    func mostSimilarIndex() -> Int
    {
        var minDistanceIndex = 0
        var maxOccurences = Int.min
        var minGlobalDistance = Int.max
        let fieldCount = self.titlesField.count
        for var i = 0 ; i < fieldCount ; i++
        {
            var minLocalDistance = Int.max
            var distanceOccurances = 0
            for var j = i+1 ; j < fieldCount ; j++
            {
                let hammingDistance = self.hammingDistanceBetweenTitles(self.titlesField[i], and: self.titlesField[j])
                if hammingDistance < minLocalDistance
                {
                    minLocalDistance = hammingDistance
                    distanceOccurances = 1
                }
                if minLocalDistance == hammingDistance
                {
                    distanceOccurances++
                }
            }
            if minGlobalDistance > minLocalDistance
            {
                minGlobalDistance = minLocalDistance
            }
            if distanceOccurances >= maxOccurences && minLocalDistance <= minGlobalDistance
            {
                maxOccurences = distanceOccurances
                minDistanceIndex = i
                print("Maybe: ", self.images[i].tearedTitle)
                print("Occurences: ", distanceOccurances)
                print("Min Local Distance is: ", minLocalDistance)
                print("Min Global Distance so far: ", minGlobalDistance)
            }
        }
        return minDistanceIndex
    }
    
    func hammingDistanceBetweenTitles(title1: [String], and title2: [String]) -> Int
    {
        var distance = 0
        let longestLength: Int = (title1.count > title2.count) ? title1.count : title2.count
        for var i = 0 ; i < longestLength ; i++
        {
            let field1 = title1.count > i ? title1[i] : ""
            let field2 = title2.count > i ? title2[i] : ""
            distance += self.hammingDistanceBetweenWords(field1, and: field2)
        }
        return distance
    }
    
    func hammingDistanceBetweenWords(w1: String, and w2: String) -> Int
    {
        let s1 = w1 as NSString
        let s2 = w2 as NSString
        let shortestLength: Int = (s1.length < s2.length) ? s1.length : s2.length
        var distance = 0
        for var i = 0 ; i < shortestLength ; i++
        {
            if s1.length <= i || s2.length <= i || s1.characterAtIndex(i) != s2.characterAtIndex(i)
            {
                distance++
            }
        }
        return distance
    }

}
