//
//  ViewController.swift
//  WikiPicia
//
//  Created by AMTourky on 2/3/16.
//  Copyright © 2016 AMTourky. All rights reserved.
//

import Cocoa
import CoreLocation

class ViewController: NSViewController, CLLocationManagerDelegate {
    
    @IBOutlet var articlesLoader: NSProgressIndicator?
    @IBOutlet var imagesLoader: NSProgressIndicator?
    
    var locationManager: CLLocationManager?
    var wikipediaAPIAction: WikipediaAPIAction?
    var similarityIdentifier: MostSimilarImageIdentifier?
    var gettingArticles: Bool?
    dynamic var articles: [Article]?
    dynamic var images: [Image]?
    dynamic var mostSimilarImage: Image?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.wikipediaAPIAction = WikipediaAPIAction(withAction: "query")
        self.gettingArticles = false
        getArticlesAroundMe()
    }
    
    @IBAction func refresh(sender: AnyObject)
    {
        self.getArticlesAroundMe()
        self.articles?.removeAll()
        self.images?.removeAll()
        self.mostSimilarImage = nil
    }
    
    func getArticlesAroundMe()
    {
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Denied ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.Restricted
        {
            self.displayLocationNoPermissionAlert()
        }
        else
        {
            self.locationManager = CLLocationManager()
            self.locationManager?.delegate = self
            self.locationManager?.startUpdatingLocation()
        }
        self.articlesLoader?.startAnimation(self)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [AnyObject])
    {
        if let currentLocation = locations.first as? CLLocation
        {
            print(currentLocation.coordinate)
            self.locationManager?.stopUpdatingLocation()
            self.getArticlesNearCoordinate(currentLocation.coordinate)
        }
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus)
    {
        if status  == CLAuthorizationStatus.Denied || status == CLAuthorizationStatus.Restricted
        {
            self.articlesLoader?.stopAnimation(self)
            self.displayLocationNoPermissionAlert()
        }
    }
    
    func displayLocationNoPermissionAlert()
    {
        let alert = NSAlert()
        alert.messageText = "Wikipicia isn't able to get your location, please enable the location service"
        alert.runModal()
    }
    
    func getArticlesNearCoordinate(coordinate: CLLocationCoordinate2D)
    {
        if let isGettingArticles = self.gettingArticles where !isGettingArticles
        {
            self.gettingArticles = true
            
            self.wikipediaAPIAction?.getArticlesAroundCoordinate(coordinate, andCallback: {
                (error, articles) -> Void in
                
                self.gettingArticles = false
                self.articlesLoader?.stopAnimation(self)
                
                guard let theArticles = articles
                else
                {
                    print("no articles error: ", error)
                    if let theError = error
                    {
                        let alert = NSAlert()
                        alert.messageText = theError.localizedDescription
                        alert.runModal()
                    }
                    return
                }
                
                self.articles = theArticles
                
                self.imagesLoader?.startAnimation(self)
                
                self.wikipediaAPIAction?.getImagesOfArticles(theArticles, andCallback: { (error, images) -> Void in
                    
                    
                    guard let theImages = images
                    else
                    {
                        print(error)
                        if let theError = error
                        {
                            let alert = NSAlert()
                            alert.messageText = theError.localizedDescription
                            alert.runModal()
                        }
                        self.imagesLoader?.stopAnimation(self)
                        return
                    }
                    self.images = theImages
                    
                    self.similarityIdentifier = MostSimilarImageIdentifier(withImages: theImages)
                    self.mostSimilarImage = self.similarityIdentifier?.theMostSimilarImage()
                    
                    self.imagesLoader?.stopAnimation(self)
                })
                
            })
        }
    }
    
    func getImageOfArticle(article: Article)
    {
        
    }
    
    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

